﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfRelationshipsCodeFirstDb
{
    class User
    {
        public long Id { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }
        [StringLength(25)]
        public string Company { get; set; }
        
        //Navigation Properties
        public virtual Password Password { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
    }
}
