namespace EfRelationshipsCodeFirstDb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<EfRelationshipsCodeFirstDb.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(EfRelationshipsCodeFirstDb.ApplicationDbContext context)
        {
            #region One-to-Many
            context.Roles.AddOrUpdate(
                    new Role
                    {
                        Id = 1,
                        UserId = 1,
                        Name = "to find the holy grail"
                    },
                    new Role
                    {
                        Id = 2,
                        UserId = 1,
                        Name = "Uh?"
                    });
            #endregion One-to-Many

            #region Many-to-Many
            List<Program> prog = new List<Program>(){new Program
                {
                    Id = 1,
                    Name = "A"
                },
                new Program
                {
                    Id = 2,
                    Name = "B"
                }};
            //context.Programs.AddOrUpdate(
            //    new Program
            //    {
            //        Id = 1,
            //        Name = "A"
            //    },
            //    new Program
            //    {
            //        Id = 2,
            //        Name = "B"
            //    });
            #endregion Many-to-Many

            #region One-to-One
            context.Users.AddOrUpdate(new User() { Id = 1, Name = "Zeeshan", Programs = prog });
            var user = new User() { Id = 2, Name = "Asad", Programs = prog };
            //context.Users.AddOrUpdate(user);

            context.Passwords.AddOrUpdate(new Password() { UserId = 1, PasswordHash = "blue", EncryptionMethod = "MD5" });
            context.Passwords.AddOrUpdate(new Password() { User = user, PasswordHash = "yellow", EncryptionMethod = "MD5" });
            #endregion One-to-One
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
