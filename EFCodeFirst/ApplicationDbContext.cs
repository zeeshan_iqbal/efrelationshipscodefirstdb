﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfRelationshipsCodeFirstDb
{
    class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base(@"Data Source=NX00432\SQLEXPRESS;Database=EfRelationshipsCodeFirstDb;
        Integrated Security=True;
        Connect Timeout=30")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Password> Passwords { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Program> Programs { get; set; }
    }
}
