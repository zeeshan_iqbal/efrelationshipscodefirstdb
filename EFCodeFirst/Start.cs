﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfRelationshipsCodeFirstDb
{
    // URI: http://www.codeproject.com/Articles/1003178/Entity-Framework-Relationships-with-Code-First-Mig
    class Start
    {
        static void Main(string[] args)
        {
            var context = new ApplicationDbContext();
            #region One-to-One
            PrintSeprator("One-to-One");
            var user = context.Users.Include("Password").First(x => x.Id == 1);
            Console.WriteLine("What is your name?");
            Console.WriteLine(user.Name);
            Console.WriteLine("What is your favorite color?");
            Console.WriteLine(user.Password.PasswordHash);
            #endregion One-to-One

            #region One-to-Many
            PrintSeprator("One-to-Many");
            user = context.Users.Include("Roles").First(x => x.Id == 1);
            Console.WriteLine("What is your quest?");
            Console.WriteLine(user.Roles.First(x => x.Id == 1).Name);
            Console.WriteLine("What is the capital of Assyria?");
            Console.WriteLine(user.Roles.First(x => x.Id == 2).Name);
            #endregion One-to-Many

            #region Many-to-Many
            PrintSeprator("Many-to-Many");
            var users = context.Users.Include("Programs").ToList();
            users.ForEach(u =>
            {
                var progs = u.Programs.ToList();
                progs.ForEach(p =>
                {
                    Console.WriteLine("User " + u.Name + " has program " + p.Name);
                });
            });


            var programs = context.Programs.Include("Users").ToList();
            programs.ForEach(p =>
            {
                var usrs = p.Users.ToList();
                usrs.ForEach(u =>
                {
                    Console.WriteLine("Program " + p.Name + " has user " + u.Name);
                });
            });
            #endregion Many-to-Many
        }

        static void PrintSeprator(string section)
        {
            Console.WriteLine(String.Format("***********************{0}***********************", section));
        }
    }
}
