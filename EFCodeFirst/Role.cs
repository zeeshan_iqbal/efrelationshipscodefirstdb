﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EfRelationshipsCodeFirstDb
{
    class Role
    {
        public long Id { get; set; }
        public long UserId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
