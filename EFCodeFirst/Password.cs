﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EfRelationshipsCodeFirstDb
{
    class Password
    {
        [Key, ForeignKey("User")]
        public long UserId { get; set; }

        [Required]
        [StringLength(256)]
        public string PasswordHash { get; set; }
        [Required]
        [StringLength(256)]
        public string EncryptionMethod  { get; set; }

        //Navigation
        public virtual User User { get; set; }
    }
}
