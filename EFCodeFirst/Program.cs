﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EfRelationshipsCodeFirstDb
{
    class Program
    {
        public long Id { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }

        //Navigation
        public virtual ICollection<User> Users { get; set; }
    }
}
